.PHONY: build clean install-deps clean-deps run

SRC := $(shell find . -name "*.moon")
OUT := $(SRC:.moon=.lua)

build: $(OUT)
clean:
	$(RM) $(OUT)

%.lua: %.moon
	@lua_modules/bin/moonc $^

install-deps: clean-deps
	mkdir lib
	cd lib && git clone https://github.com/excessive/cpml.git

	./luarocks install moonscript
	./luarocks install readosm

	# Broken for the moment:
	# ./luarocks install --server=https://luarocks.org/dev cpml

	# ./luarocks install tween

clean-deps:
	$(RM) -r lib lua_modules

run: build
	love .
