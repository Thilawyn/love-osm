package.path = "lib/?.lua;" ..
               "lib/?/init.lua;" ..
               "lua_modules/share/lua/5.1/?.lua;" ..
               "lua_modules/share/lua/5.1/?/init.lua;" ..
               package.path

package.cpath = "lib/?.so;" ..
                "lua_modules/lib/lua/5.1/?.so;" ..
                package.cpath
