require "paths"
export love


import p from require "moon"
import vec2 from require "cpml"

import OsmMap from require "loveosm.osm.OsmMap"
import MercatorProjection from require "loveosm.projections.MercatorProjection"


brignoud = OsmMap
	file: "maps/brignoud.osm"

map = brignoud\toMap MercatorProjection
	center: vec2 5.9075, 45.2597
			  --  lng      lat
	scale:  2


-- import Map from require "loveosm.map.Map"
--
-- import Node from require "loveosm.map.Node"
-- import Path from require "loveosm.map.objects.paths.Path"
--
--
-- -- map = Map!
-- --
-- -- path1 = Path
-- -- 	:map
-- --
-- -- 	shape: Path.SHAPES.CURVED
-- --
-- -- 	from_node: Node
-- -- 		:map
-- -- 		position: vec2 100, 100
-- --
-- -- 	to_node: Node
-- -- 		:map
-- -- 		position: vec2 500, 500
-- --
-- -- 	control_points: {
-- -- 		vec2 500, 100
-- -- 	}

map\repaint!


export love

-- Window dimensions
dimensions = vec2 love.graphics.getDimensions!
half_dimensions = dimensions * 0.5

love.resize = (w, h) ->
	with dimensions
		.x = w
		.y = h

	with half_dimensions
		.x = w * 0.5
		.y = h * 0.5

-- Draw the map
love.draw = ->
	map\draw!

-- Mouse position
mouse_pos = vec2 love.mouse.getPosition!

-- Manage map movement
RIGHT_CLICK_MAX_ACCEL = 1000  -- In pixels per second


left_click_moving = false
right_click_moving = false
right_click_moving_origin = vec2 0, 0


love.mousepressed = (x, y, button, is_touch, presses) ->
	if button == 1
		left_click_moving = true
	if button == 2
		right_click_moving = true

		with right_click_moving_origin
			.x = x
			.y = y

love.mousereleased = (x, y, button, is_touch, presses) ->
	if button == 1
		left_click_moving = false
	if button == 2
		right_click_moving = false

love.mousemoved = (x, y, dx, dy, is_touch) ->
	with mouse_pos
		.x = x
		.y = y

	if left_click_moving
		with map.camera.position
			.x += -dx * map.camera.scale
			.y += -dy * map.camera.scale

love.wheelmoved = (x, y) ->
	if y < 0
		map.camera.scale *= 0.5
	if y > 0
		map.camera.scale *= 2

love.update = (dt) ->
	if right_click_moving
		accel_coeff = (mouse_pos - right_click_moving_origin) / half_dimensions

		with map.camera.position
			.x += RIGHT_CLICK_MAX_ACCEL * dt * accel_coeff.x
			.y += RIGHT_CLICK_MAX_ACCEL * dt * accel_coeff.y
