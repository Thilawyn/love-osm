import vec2 from require "cpml"
import
	rad
	min
	max
	sqrt
	sin
	tan
	pi
	log
from math

import Projection from require "loveosm.projections.Projection"


class MercatorProjection extends Projection

	@R_MAJOR:      6378137.0
	@R_MINOR:      6356752.3142

	@RATIO:        @R_MINOR / @R_MAJOR

	@ECCENTRICITY: sqrt 1 - @@RATIO * @@RATIO
	@COM:          0.5 * @@ECCENTRICITY


	toX: (lng) =>
		@translateX @@R_MAJOR * rad lng

	toY: (lat) =>
		lat = min 89.6, max lat, -89.5
		phi = rad lat

		con = @@ECCENTRICITY * sin phi
		con = ((1 - con) / (1 + con)) ^ @@COM

		ts  = (tan 0.5 * (pi * 0.5 - phi)) / con

		@translateY 0 - @@R_MAJOR * log ts

	toXY: (lng, lat) =>
		vec2 (@toX lng), (@toY lat)


:MercatorProjection
