class Projection

	new: do
		import vec2 from require "cpml"

		(attributes = {}) =>
			@scale  = 1
			@center = vec2 0, 0

			@center = @toXY attributes.center.x, attributes.center.y if attributes.center
			@scale  = attributes.scale or 1

	toX: (lng) =>
	toY: (lat) =>
	toXY: (lng, lat) =>

	translateX: (x) => (x - @center.x) * @scale
	translateY: (y) => (y - @center.y) * @scale


:Projection
