-- An OSM map, parsed from an osm/pbf file
class OsmMap

	new: (attributes = {}) =>
		@file = attributes.file or error "An OSM file must be specified"

	-- Parses the data from the osm file using readosm, then stores the nodes that have been returned
	toMap: do
		readosm = require "readosm"
		import Map from require "loveosm.map.Map"

		import tostring, pairs from _G

		(projection) =>
			@projection = projection

			-- Create a new map
			@map = Map!

			-- Open the OSM file
			f = readosm.open @file

			-- Parse it
			@node_structs = {}
			@node_objects = {}

			f\parse (t, obj) ->
				switch t
					-- Convert nodes
					when "node"
						@node_structs[tostring obj.id] = obj

					-- Convert ways
					when "way"
						for k, v in pairs obj.tags
							-- If there's an highway tag, then it's a path
							if k == "highway"
								@createPath obj
								break

					-- Store relations
					-- when "relation"
					-- 	@relations[obj.id] = obj

			-- Then close it
			f\close!

			@map


	----------------------------------
	-- Map conversion
	----------------------------------

	getNode: do
		import Node from require "loveosm.map.Node"

		(id) =>
			node = @node_objects[id]

			-- If the node doesn't exist yet, create it
			if not node
				obj  = @node_structs[id]
				node = Node
					map:      @map
					position: @projection\toXY obj.longitude, obj.latitude

				@node_objects[id] = node

			node

	createPath: do
		import pairs, tostring from _G
		import Path from require "loveosm.map.objects.paths.Path"

		(obj) =>
			local previous_node

			print "----"
			for _, node_id in pairs obj.node_refs
				node = @getNode tostring node_id
				print node_id, node, node.position, previous_node

				if previous_node
					Path
						map:       @map
						from_node: previous_node
						to_node:   node

				previous_node = node


:OsmMap
