class MapObject
	new: (attributes = {}) =>
		@map = attributes.map

		@map\addObject @

	destroy: =>
		@map\removeObject @

	repaint: =>


:MapObject
