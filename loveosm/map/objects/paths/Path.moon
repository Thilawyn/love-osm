import MapObject from require "loveosm.map.objects.MapObject"
import insert from table

export love
import math, graphics from love


class Path extends MapObject

	@SHAPES:
		STRAIGHT: 1
		CURVED:   2


	new: (attributes = {}) =>
		super attributes

		@shape     = attributes.shape     or @@SHAPES.STRAIGHT
		@from_node = attributes.from_node or error "A start node must be specified"
		@to_node   = attributes.to_node   or error "An end node must be specified"

		if @shape == @@SHAPES.CURVED
			@control_points = attributes.control_points or {}

		-- Connect this path to the start node
		@from_node\addPath @

	repaint: =>
		graphics.setColor 0, 0, 0

		switch @shape
			when @@SHAPES.STRAIGHT
				graphics.line @from_node.position.x, @from_node.position.y, @to_node.position.x, @to_node.position.y

			when @@SHAPES.CURVED
				@curve = math.newBezierCurve @from_node.position.x, @from_node.position.y, @to_node.position.x, @to_node.position.y

				for i = 1, #@control_points
					@curve\insertControlPoint @control_points[i].x, @control_points[i].y

				graphics.line @curve\render!

				for i = 1, @curve\getControlPointCount!
					x, y = @curve\getControlPoint i

					graphics.setColor 1, 0, 0
					graphics.circle "fill", x, y, 5

					graphics.setColor 0, 0, 0
					graphics.print "#{ i } (#{ x }, #{ y })", x, y - 20


:Path
