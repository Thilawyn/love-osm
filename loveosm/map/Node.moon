class Node

	----------------------------------
	-- Constructor and destroyer
	----------------------------------
	new: (attributes = {}) =>
		@map      = attributes.map      or error "A map must me be specified"
		@position = attributes.position or error "A position must be specified"

		-- Connected paths
		@paths = {}

		-- Add the node to the map
		index = #@map.nodes + 1

		@index = index
		@map.nodes[index] = @

	destroy: do
		import remove from table

		=>
			-- Remove the node from the map
			remove @map.nodes, @index

			-- Update the indexes
			@updateNodesIndex @index

			@index = nil

	updateNodesIndex: (from_i = 1) =>
		for i = from_i, #@map.nodes
			@map.nodes[i].index = i


	----------------------------------
	-- Paths
	----------------------------------
	addPath: (path) =>
		@paths[#@paths + 1] = path


:Node
