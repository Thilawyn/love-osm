export love
import graphics from love

import remove from table


class Map
	new: do
		import vec2 from require "cpml"
		import Camera from require "loveosm.map.Camera"

		(attributes = {}) =>
			@size  = attributes.size  or vec2 10000, 10000
			@scale = attributes.scale or 1

			@nodes   = {}
			@objects = {}

			@camera = Camera!
			@canvas = graphics.newCanvas @size.x, @size.y

	addObject: (obj) =>
		index = #@objects + 1

		obj.index = index
		@objects[index] = obj

	removeObject: (obj) =>
		remove @objects, obj.index

	toCanvasXY: (coords) =>
		coords + @size * 0.5

	repaint: =>
		graphics.setCanvas @canvas

		graphics.setBackgroundColor 1, 1, 1
		graphics.clear!

		-- Set the coordinate system
		graphics.push "all"

		center = @size * 0.5
		graphics.translate center.x, center.y

		-- Repaint all objects
		for i = 1, #@objects
			@objects[i]\repaint!

		-- Repaint all nodes
		-- for i = 1, #@nodes
		-- 	graphics.setColor 0, 0, 0
		-- 	graphics.circle "fill", @nodes[i].position.x, @nodes[i].position.y, 5

		-- Reset the coordinate system
		graphics.pop!

		graphics.setCanvas!

	draw: =>
		-- Set the coordinate system
		graphics.push "all"

		graphics.scale     @camera.scale
		graphics.translate -@camera.position.x, -@camera.position.y
		graphics.translate -@size.x * 0.5, -@size.y * 0.5

		-- Draw the canvas
		graphics.setColor 1, 1, 1
		graphics.draw @canvas, 0, 0

		-- Reset the coordinate system
		graphics.pop!


:Map
